package com.crypto.cryptoservice.client;

import com.crypto.cryptoservice.dto.QuoteDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("databaseservice")
public interface DatabaseClient {

  @GetMapping("/quotes/{username}")
  List<String> getUserQuotes(@PathVariable("username") final String username);

  @GetMapping("/quotes/retrieveAllUsers")
  List<String> fetchAllUsers();

  @PostMapping("/quotes/addQuote")
  void addQuote(@RequestBody final QuoteDTO quoteDTO);

  @PostMapping("/quotes/deleteQuote")
  void deleteQuote(@RequestBody final QuoteDTO quoteDTO);

  @DeleteMapping("/quotes/deleteUserQuotes/{username}")
  void deleteUserQuotes(@PathVariable("username") final String username);

}
