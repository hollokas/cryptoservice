package com.crypto.cryptoservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.LinkedHashMap;

@Data
public class CryptoDataDTO {

  @JsonProperty("Meta Data")
  private MetaDataDTO metaDataDTO;

//
//  @JsonProperty("Time Series (Digital Currency Intraday)")
//  private HashMap<String, PriceDTO> timeSeries;

  @JsonProperty("Time Series (Digital Currency Daily)")
  private LinkedHashMap<String, PriceDTO> timeSeries;

}
