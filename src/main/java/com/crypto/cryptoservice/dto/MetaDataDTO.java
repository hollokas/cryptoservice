package com.crypto.cryptoservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MetaDataDTO {

  @JsonProperty("2. Digital Currency Code")
  private String currencyCode;

  @JsonProperty("3. Digital Currency Name")
  private String currencyName;

  @JsonProperty("6. Last Refreshed")
  private String lastRefreshed;

}
