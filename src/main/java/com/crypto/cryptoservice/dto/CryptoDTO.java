package com.crypto.cryptoservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class CryptoDTO {

  private String currencyCode;

  private String currencyName;

}
