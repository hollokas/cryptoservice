package com.crypto.cryptoservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


public class PriceDTO {

  private double priceEUR;

  private double priceUSD;

  @JsonProperty("EUR")
  public double getPriceEUR() {
    return priceEUR;
  }

  @JsonProperty("1a. open (EUR)")
  public void setPriceEUR(double priceEUR) {
    this.priceEUR = priceEUR;
  }

  @JsonProperty("USD")
  public double getPriceUSD() {
    return priceUSD;
  }

  @JsonProperty("1b. open (USD)")
  public void setPriceUSD(double priceUSD) {
    this.priceUSD = priceUSD;
  }

}
