package com.crypto.cryptoservice.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@Data
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class CryptoDetailsDTO {

    private String currencyName;

    private String currencyCode;

    private String lastRefreshed;

    private BigDecimal lastPriceEUR;

    private BigDecimal lastPriceUSD;

    private Map<String, PriceDTO> currencyChanges;
}
