package com.crypto.cryptoservice.service;

import com.crypto.cryptoservice.client.DatabaseClient;
import com.crypto.cryptoservice.dto.*;
import com.crypto.cryptoservice.dto.PriceDTO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CryptoService {

  @Autowired
  DatabaseClient databaseClient;

  private CryptoDataDTO getCryptoDataDTO(RestTemplate restTemplate, String currencyCode) {
    return restTemplate.exchange(
        "https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=" + currencyCode + "&market=EUR&apikey=3U6S3RDG6R7A72KU",
        HttpMethod.GET, null, CryptoDataDTO.class)
        .getBody();
  }

  public List<CryptoDetailsDTO> retrieveCrypto(String username) {


    RestTemplate restTemplate = new RestTemplate();

    //Check if user exists. It might turn out that there are no such users.
    List<String> currencyCodes = databaseClient.getUserQuotes(username);

    List<CryptoDetailsDTO> cryptoDetailsDTOS = currencyCodes.stream()
        .map(currencyCode -> {
              //Retrieve currency data from Alpha Vantage API
              CryptoDataDTO cryptoDataDTO = getCryptoDataDTO(restTemplate, currencyCode);

              CryptoDetailsDTO cryptoDetailsDTO;

              if (cryptoDataDTO.getMetaDataDTO() != null && cryptoDataDTO.getTimeSeries() != null) {

                String lastRefreshed = cryptoDataDTO.getMetaDataDTO().getLastRefreshed().substring(0, cryptoDataDTO.getMetaDataDTO().getLastRefreshed().indexOf(" "));
                //Convert data into CryptoDetailsDTO type
                cryptoDetailsDTO = CryptoDetailsDTO.of(
                    cryptoDataDTO.getMetaDataDTO().getCurrencyName(),
                    currencyCode,
                    lastRefreshed,
                    BigDecimal.valueOf(cryptoDataDTO.getTimeSeries().get(lastRefreshed).getPriceEUR()),
                    BigDecimal.valueOf(cryptoDataDTO.getTimeSeries().get(lastRefreshed).getPriceUSD()),
                    cryptoDataDTO.getTimeSeries()
                );

                return cryptoDetailsDTO;
              } else {

                //If a currency code does not exist
                cryptoDetailsDTO = CryptoDetailsDTO.of(
                    null, currencyCode, null, null, null, null
                );

                return cryptoDetailsDTO;

              }
            }
        ).collect(Collectors.toList());

    return cryptoDetailsDTOS;

  }

  public List<CryptoDTO> fetchAllCryptoCodes() {

    List<CryptoDTO> cryptoDTOs = new ArrayList<>();

    try {
      Reader input = new FileReader("digital_currency_list.csv");

      Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().withSkipHeaderRecord().parse(input);

      records
          .forEach(record -> {
            CryptoDTO cryptoDTO = CryptoDTO.of(record.get(0),record.get(1));
            cryptoDTOs.add(cryptoDTO);
          });

    } catch (IOException e) {
      e.printStackTrace();
    }

    return cryptoDTOs;
  }

  public void addQuote(QuoteDTO quoteDTO) {

    databaseClient.addQuote(quoteDTO);

  }

  public void deleteQuote(QuoteDTO quoteDTO) {

    databaseClient.deleteQuote(quoteDTO);

  }

  public List<String> fetchAllUsers() {

    return databaseClient.fetchAllUsers();

  }

  public void deleteUserQuotes(String username) {

    databaseClient.deleteUserQuotes(username);

  }
}
