package com.crypto.cryptoservice.rest;

import com.crypto.cryptoservice.dto.CryptoDTO;
import com.crypto.cryptoservice.dto.QuoteDTO;
import com.crypto.cryptoservice.dto.CryptoDetailsDTO;
import com.crypto.cryptoservice.service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cryptos")
public class CryptoRestController {

  @Autowired
  private CryptoService cryptoService;

  @GetMapping("/{username}")
  public ResponseEntity<List<CryptoDetailsDTO>> retrieveQuote(@PathVariable("username") final String username) {

    List<CryptoDetailsDTO> cryptoDetailsDTOS = cryptoService.retrieveCrypto(username);

    return new ResponseEntity<>(cryptoDetailsDTOS, HttpStatus.OK);
  }

  @GetMapping("/fetchAllCryptoCodes")
  public ResponseEntity<List<CryptoDTO>> fetchAllCryptoCodes() {

    List<CryptoDTO> cryptoDTOs = cryptoService.fetchAllCryptoCodes();

    return new ResponseEntity<>(cryptoDTOs, HttpStatus.OK);
  }

  @GetMapping("/fetchAllUsers")
  public ResponseEntity<List<String>> fetchAllUsers() {

    List<String> users = cryptoService.fetchAllUsers();

    return new ResponseEntity<>(users, HttpStatus.OK);
  }

  @PostMapping("/addQuote")
  public void addQuote(@RequestBody final QuoteDTO quoteDTO) {

    cryptoService.addQuote(quoteDTO);

  }

  @PostMapping("/deleteQuote")
  public void deleteQuote(@RequestBody final QuoteDTO quoteDTO) {

    cryptoService.deleteQuote(quoteDTO);

  }

  @DeleteMapping("/deleteUserQuotes/{username}")
  public void deleteUserQuotes(@PathVariable("username") final String username) {

    cryptoService.deleteUserQuotes(username);

  }


}
